import * as fs from 'fs-extra'
import * as os from 'os'
import * as path from 'path'
import extract from 'extract-zip'
import * as xpath from 'xpath'
import { DOMParser } from 'xmldom'
import { Table } from './elements/table'
import { isElement } from './elements/guards'
import type { DBTable } from './elements/table'

export class Parse {
	static async parseAsync(path: string) {
		return new Parse(path).parseTable()
	}

	constructor(public readonly path: string) {}

	async parseTable() {
		return withTemporaryDirectory(async workDirectory => {
			const statusExtractArchive = await extractArchive(
				this.path,
				workDirectory,
			)
			if (statusExtractArchive) return parseXml(workDirectory)
		})
	}
}

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
if (!Parse.name) Parse.name = 'ParseMwb'

const parseXml = async (dir: string): Promise<readonly DBTable[]> => {
	const xml = await fs.readFile(path.join(dir, 'document.mwb.xml'), 'utf8')
	const domData = new DOMParser().parseFromString(xml)
	const nodeTables = xpath.select(
		'//value[@struct-name="db.mysql.Table"]',
		domData,
	)
	const tables: Table[] = []
	const tableIds: Record<string, Table> = {}
	for (const eTable of nodeTables) {
		if (!isElement(eTable)) continue
		const dataTable = new Table(eTable)
		tables.push(dataTable)
		tableIds[dataTable.id] = dataTable
	}
	for (const table of tables) {
		table.resolveForeignKeyReference(tableIds)
	}
	return tables
}

const extractArchive = async (pathFile: string, dir: string) => {
	try {
		await extract(pathFile, { dir })
	} catch {
		return false
	}
	return true
}

const withTemporaryDirectory = async <T>(fn: (dir: string) => Promise<T>) => {
	const name = 'mwb_' + Date.now() + '_' + Math.floor(Math.random() * 100000)
	const dir = path.join(os.tmpdir(), name)
	await fs.remove(dir).catch(e => console.error(e))
	await fs.mkdir(dir)
	try {
		const r = await fn(dir)
		return r
	} finally {
		await fs.remove(dir).catch(e => console.error(e))
	}
}

export default Parse
