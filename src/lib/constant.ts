export const SQL_TABLE_STAT = 0
export const SQL_CASCADE = 0
export const SQL_RESTRICT = 1
export const SQL_SET_NULL = 2
export const SQL_NO_ACTION = 3
export const SQL_SET_DEFAULT = 4

export const fkRoleToString = (rulenum: number) => {
	switch (rulenum) {
		case SQL_CASCADE:
			return 'CASCADE'
		case SQL_RESTRICT:
			return 'RESTRICT'
		case SQL_SET_NULL:
			return 'SET NULL'
		case SQL_NO_ACTION:
			return 'NO ACTION'
		case SQL_SET_DEFAULT:
			return 'SET DEFAULT'
		default:
			return ''
	}
}

export type ForeignKeyRule = ReturnType<typeof fkRoleToString>
