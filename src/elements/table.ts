import * as xpath from 'xpath'
import Base from './base'
import { Column } from './column'
import { Index } from './index'
import { ForeignKey } from './foreign_key'
import { isElement } from './guards'
import type { DBColumn } from './column'
import type { DBIndex } from './index'
import type { DBForeignKey } from './foreign_key'

export interface DBTable {
	readonly id: string
	readonly name: string
	readonly columns: readonly DBColumn[]
	readonly indexes: readonly DBIndex[]
	readonly foreignKeys: readonly DBForeignKey[]
}

export class Table implements DBTable {
	readonly id: string
	readonly name: string
	readonly columns: Column[]
	readonly indexes: Index[]
	readonly foreignKeys: ForeignKey[]
	private readonly base: Base

	toJSON() {
		return {
			id: this.id,
			name: this.name,
			columns: this.columns,
			indexes: this.indexes,
			foreignKeys: this.foreignKeys,
		}
	}

	constructor(table: Element) {
		this.id = ''
		this.name = ''
		this.columns = []
		this.indexes = []
		this.foreignKeys = []
		this.base = new Base(table)
		this.parse()
	}

	private setProp<K extends keyof Table>(key: K, val: Table[K]) {
		// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
		// @ts-ignore
		this[key] = val
	}

	private parse() {
		this.setProp('id', this.base.getId())
		this.setProp('name', this.base.getString('name'))
		this.parseColumns()
		const ids: Record<string, Column> = {}
		for (const column of this.columns) {
			ids[column.id] = column
		}
		this.parseIndexes(ids)
		return this
	}

	/**
	 * parse to columns
	 *
	 * @return {Array<*>}
	 */
	private parseColumns() {
		const columes = xpath.select(
			'.//value[@struct-name="db.mysql.Column"]',
			this.base.object,
		)
		if (columes && Array.isArray(columes) && columes.length > 0) {
			for (const eComue of columes) {
				if (!isElement(eComue)) continue
				this.columns.push(new Column(eComue))
			}
		}
	}

	/**
	 * Parse to indexs
	 *
	 * @returns {Array<*>}
	 */
	private parseIndexes(ids: Record<string, Column>) {
		const indexes = xpath.select(
			'.//value[@struct-name="db.mysql.Index"]',
			this.base.object,
		)
		if (indexes && Array.isArray(indexes) && indexes.length) {
			for (const eIndex of indexes) {
				if (!isElement(eIndex)) continue
				const index = new Index(eIndex)
				index.resolveColumns(ids)
				this.indexes.push(index)
			}
		}
	}

	/**
	 * Parse list ForeignKey
	 *
	 */
	private parseForeignKey(ids: Record<string, Column>) {
		const foreignKeys = xpath.select(
			'.//value[@struct-name="db.mysql.ForeignKey"]',
			this.base.object,
		)
		for (const eforeignKey of foreignKeys) {
			if (!isElement(eforeignKey)) continue
			const foreignKeyElement = new ForeignKey(eforeignKey)
			foreignKeyElement.resolveColumns(ids)
			this.foreignKeys.push(foreignKeyElement)
		}
	}

	/**
	 * @param {Array}
	 */
	resolveForeignKeyReference(tables: Record<string, Table>) {
		for (const foreignKey of this.foreignKeys) {
			foreignKey.resolveReferencedTableAndColumn(tables)
		}
	}

	getColumnById(id: string) {
		for (const column of this.columns) {
			if (column.id === id) {
				return column
			}
		}
		return null
	}
}

export default Table
