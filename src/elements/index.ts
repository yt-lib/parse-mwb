import * as xpath from 'xpath'
import Base from './base'
import { isElement } from './guards'
import type { Column, DBColumn } from './column'

export interface DBIndex {
	readonly id: string
	readonly primary: boolean
	readonly unique: boolean
	readonly columns: readonly DBColumn[]
	readonly name: string
	readonly comment: string
}

export class Index implements DBIndex {
	private readonly base: Base
	readonly id: string
	readonly primary: boolean
	readonly unique: boolean
	readonly columns: Column[]
	private columnIds: string[]
	readonly name: string
	readonly comment: string

	toJSON() {
		return {
			id: this.id,
			primary: this.primary,
			unique: this.unique,
			columns: this.columns,
			name: this.name,
			comment: this.comment,
		}
	}

	constructor(index: Element) {
		this.base = new Base(index)
		this.id = ''
		this.primary = false
		this.unique = false
		this.columns = []
		this.columnIds = []
		this.name = ''
		this.comment = ''
		this.parse()
	}

	private setProp<K extends keyof Index>(key: K, val: Index[K]) {
		// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
		// @ts-ignore
		this[key] = val
	}

	private parse() {
		this.setProp('id', this.base.getId())
		this.columnIds = []
		const columns = xpath.select(
			'.//value[@struct-name="db.mysql.IndexColumn"]',
			this.base.object,
		)
		if (columns && Array.isArray(columns) && columns.length > 0) {
			for (const column of columns) {
				if (!isElement(column)) continue
				const ids = xpath.select('link[@struct-name="db.Column"]', column)
				for (const id of ids) {
					if (isElement(id) && id.childNodes[0]) {
						const val = id.childNodes[0].nodeValue
						if (val) this.columnIds.push(val)
					}
				}
			}
		}
		this.parseSpecificAttributes()
	}

	/**
	 * parseSpecificAttributes
	 */
	private parseSpecificAttributes() {
		this.setProp('name', this.base.getString('name'))
		this.setProp('primary', 1 === this.base.getValue('isPrimary'))
		this.setProp('unique', 1 === this.base.getValue('unique'))
	}

	resolveColumns(columns: Record<string, Column>) {
		if (columns)
			for (const id of this.columnIds) {
				if (columns[id]) {
					this.columns.push(columns[id])
				}
			}
	}
}

export default Index
