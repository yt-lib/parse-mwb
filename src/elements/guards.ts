/* eslint-disable @typescript-eslint/no-explicit-any */

export const isObject = (v: unknown): v is object =>
	'object' === typeof v && !!v

export const isRecord = (v: unknown): v is Record<keyof any, unknown> =>
	isObject(v) && !Array.isArray(v)

const ELEMENT_NODE = 1
export const isElement = (v: unknown): v is Element =>
	isRecord(v) && ELEMENT_NODE === v.nodeType
