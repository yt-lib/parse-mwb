import * as xpath from 'xpath'
import { isElement } from './guards'

export class Base {
	readonly object: Element

	constructor(table: Element) {
		this.object = table
		return this
	}

	/**
	 * Get value of element by id
	 *
	 * @returns {String<*>}
	 */
	getId() {
		const id = this.object.getAttribute('id')
		if (id) return id
		throw new Error('no id found.')
	}

	getKeys(): readonly string[] {
		const attrs = xpath.select('value/@key', this.object)
		const keys: string[] = []
		for (const attr of attrs)
			if ('object' === typeof attr && attr.nodeValue) keys.push(attr.nodeValue)
		return keys
	}

	getLinkKeys(): readonly string[] {
		const attrs = xpath.select('link/@key', this.object)
		const keys: string[] = []
		for (const attr of attrs)
			if ('object' === typeof attr && attr.nodeValue) keys.push(attr.nodeValue)
		return keys
	}

	getValues() {
		const rec: Record<string, string | number | undefined> = {}
		for (const key of this.getKeys()) {
			const val = this.getValue(key)
			if (null != val) rec[key] = val
		}
		return rec
	}

	getLinks() {
		const rec: Record<string, string | undefined> = {}
		for (const key of this.getLinkKeys()) {
			const val = this.getLink(key)
			if (null != val) rec[key] = val
		}
		return rec
	}

	getAttributes() {
		const rec: Record<string, string> = {}
		for (const a of Array.from(this.object.attributes)) rec[a.name] = a.value
		return rec
	}

	getValue(key: string, defaultString?: string) {
		const element = xpath.select1('value[@key="' + key + '"]', this.object)
		if (isElement(element) && element.childNodes[0]?.nodeValue) {
			if ('int' === element.getAttribute('type')) {
				return parseInt(element.childNodes[0].nodeValue)
			}
			return element.childNodes[0].nodeValue
		}
		return defaultString
	}

	getString(key: string, defaultString?: string) {
		return this.getValue(key, defaultString)?.toString() || ''
	}

	getNumber(key: string) {
		const num = this.getValue(key)
		if ('number' === typeof num) return num
		if ('string' === typeof num && /^-?\d+$/.test(num)) return Number(num)
		throw new Error(`${key} must be number.`)
	}

	/**
	 * Get link from mwb in table
	 *
	 * @param {*} key
	 * @return {String<*>}
	 */
	getLink(key: string) {
		const element = xpath.select1('link[@key="' + key + '"]', this.object)
		if (isElement(element) && element.childNodes[0]?.nodeValue) {
			return element.childNodes[0].nodeValue
		}
		return null
	}
}

export default Base
