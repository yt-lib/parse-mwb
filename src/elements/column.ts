import * as xpath from 'xpath'
import Base from './base'
import { isElement } from './guards'

import { isDataType } from '../lib/datatype'
import type { DataType } from '../lib/datatype'

export interface DBColumn {
	readonly id: string
	readonly name: string
	readonly nullable: boolean
	readonly defaultValue: string
	readonly autoIncrement: boolean
	readonly type: DataType
	readonly length: number
	readonly precision: number
	readonly scale: number
	readonly unsigned: boolean
	readonly _attributes: Record<string, string | undefined>
	readonly _values: Record<string, string | number | undefined>
	readonly _links: Record<string, string | undefined>
}

export class Column implements DBColumn {
	private readonly base: Base
	readonly id: string
	readonly name: string
	readonly nullable: boolean
	readonly defaultValue: string
	readonly autoIncrement: boolean
	readonly type: DataType
	readonly length: number
	readonly precision: number
	readonly scale: number
	readonly unsigned: boolean

	toJSON() {
		return {
			id: this.id,
			name: this.name,
			nullable: this.nullable,
			defaultValue: this.defaultValue,
			autoIncrement: this.autoIncrement,
			type: this.type,
			length: this.length,
			precision: this.precision,
			scale: this.scale,
			unsigned: this.unsigned,
			_attributes: this.base.getAttributes(),
			_values: this.base.getValues(),
			_links: this.base.getLinks(),
		}
	}

	constructor(eColume: Element) {
		this.base = new Base(eColume)
		// this.attributes = {}
		this.id = ''
		this.name = ''
		this.nullable = false
		this.defaultValue = ''
		this.autoIncrement = false
		this.type = ''
		this.length = 0
		this.precision = 0
		this.scale = 0
		this.unsigned = false
		this.parse()
	}

	private setProp<K extends keyof Column>(key: K, val: Column[K]) {
		// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
		// @ts-ignore
		this[key] = val
	}

	get _values() {
		return this.base.getValues()
	}
	get _links() {
		return this.base.getLinks()
	}

	get _attributes() {
		return this.base.getAttributes()
	}

	/**
	 * Parse to column
	 *
	 * @return {Object<*>}
	 */
	private parse() {
		this.setProp('id', this.base.getId())
		this.parseFlags()
		this.parseSpecificAttributes()
		this.parseType()
		return this
	}

	/**
	 * Parse flags
	 *
	 */
	private parseFlags() {
		this.setProp('unsigned', false)
		const flags = xpath.select('value[@key="flags"]', this.base.object)
		if (flags && Array.isArray(flags) && flags.length > 0) {
			for (const flag of flags) {
				if (!isElement(flag)) continue
				const values = xpath.select('value', flag)
				if (values && Array.isArray(values) && values.length > 0) {
					for (const value of values) {
						if (
							isElement(value) &&
							'UNSIGNED' === value.childNodes[0]?.nodeValue
						) {
							this.setProp('unsigned', true)
						}
					}
				}
			}
		}
	}

	private parseSpecificAttributes() {
		this.setProp('name', this.base.getString('name'))
		this.setProp('nullable', this.base.getValue('isNotNull') !== 1)
		this.setProp('defaultValue', this.base.getString('defaultValue'))
		this.setProp('autoIncrement', this.base.getNumber('autoIncrement') > 0)
		this.setProp('length', this.base.getNumber('length'))
		this.setProp('precision', this.base.getNumber('precision'))
		this.setProp('scale', this.base.getNumber('scale'))
		// this.setProp('attributes', this.base.getAttributes())
	}

	private parseType() {
		const typeLink = this.base.getLink('simpleType')
		const types = typeLink?.split('.')
		const type = types?.[types.length - 1] || ''
		if (isDataType(type)) this.setProp('type', type)
		else throw new Error('unknown datatype.')
	}
}

export default Column
