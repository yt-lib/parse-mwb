import Base from './base'
import * as xpath from 'xpath'
import { isElement } from './guards'
import type { Column, DBColumn } from './column'
import type { Table } from './table'
import { fkRoleToString } from '../lib/constant'
import type { ForeignKeyRule } from '../lib/constant'

export interface DBForeignKey {
	readonly id: string
	readonly name: string
	readonly many: boolean
	readonly columns: readonly DBColumn[]
	readonly referencedTableName: string
	readonly referencedColumns: readonly DBColumn[]
	readonly referencedTableId: string
	readonly updateRule: ForeignKeyRule
	readonly deleteRule: ForeignKeyRule
}

export class ForeignKey implements DBForeignKey {
	private readonly base: Base
	readonly id: string
	readonly name: string
	readonly many: boolean
	readonly columns: Column[]
	private columnIds: string[]
	readonly referencedTableName: string
	readonly referencedColumns: Column[]
	private referencedColumnIds: string[]
	readonly referencedTableId: string
	readonly updateRule: ForeignKeyRule
	readonly deleteRule: ForeignKeyRule

	toJSON() {
		return {
			id: this.id,
			name: this.name,
			many: this.many,
			columns: this.columns,
			referencedTableName: this.referencedTableName,
			referencedColumns: this.referencedColumns,
			referencedTableId: this.referencedTableId,
			updateRule: this.updateRule,
			deleteRule: this.deleteRule,
		}
	}

	constructor(eForeignKey: Element) {
		this.base = new Base(eForeignKey)
		this.id = ''
		this.name = ''
		this.many = false
		this.columns = []
		this.columnIds = []
		this.referencedTableName = ''
		this.referencedColumns = []
		this.referencedColumnIds = []
		this.referencedTableId = ''
		this.updateRule = ''
		this.deleteRule = ''
		this.parse()
	}

	private setProp<K extends keyof ForeignKey>(key: K, val: ForeignKey[K]) {
		// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
		// @ts-ignore
		this[key] = val
	}

	private parse() {
		this.setProp('id', this.base.getId())
		const columns = xpath.select('.//value[@key="columns"]', this.base.object)
		this.columnIds = []
		this.referencedColumnIds = []
		if (columns && Array.isArray(columns) && columns.length > 0) {
			for (const column of columns) {
				if (!isElement(column)) continue
				const ids = xpath.select('link[@type="object"]', column)
				for (const id of ids) {
					if (isElement(id) && id.childNodes[0]) {
						const val = id.childNodes[0].nodeValue
						val && this.columnIds.push(val)
					}
				}
			}
		}
		const referencedColumns = xpath.select(
			'.//value[@key="referencedColumns"]',
			this.base.object,
		)
		if (
			referencedColumns &&
			Array.isArray(referencedColumns) &&
			referencedColumns.length
		) {
			for (const column of referencedColumns) {
				if (!isElement(column)) continue
				const ids = xpath.select('link[@type="object"]', column)
				for (const id of ids) {
					if (isElement(id) && id.childNodes[0]) {
						const val = id.childNodes[0].nodeValue
						if (val) this.referencedColumnIds.push(val)
					}
				}
			}
		}
		this.parseSpecificAttributes()
	}

	private parseSpecificAttributes() {
		const { base } = this
		this.setProp('name', base.getString('name'))
		this.setProp('many', 1 === base.getValue('many'))
		this.setProp('referencedTableId', base.getString('referencedTable'))
		this.setProp('deleteRule', fkRoleToString(base.getNumber('deleteRule')))
		this.setProp('updateRule', fkRoleToString(base.getNumber('updateRule')))
	}

	resolveColumns(columns: Record<string, Column>) {
		if (columns)
			for (const id of this.columnIds) {
				if (columns[id]) {
					this.columns.push(columns[id])
				}
			}
	}

	resolveReferencedTableAndColumn(tables: Record<string, Table>) {
		this.setProp('referencedTableName', '')
		this.referencedColumnIds = []
		if (tables && tables[this.referencedTableId]) {
			const table = tables[this.referencedTableId]
			for (const referencedColumnId of this.referencedColumnIds) {
				const column = table.getColumnById(referencedColumnId)
				if (column) {
					this.referencedColumns.push(column)
				}
			}
		}
	}
}

export default ForeignKey
