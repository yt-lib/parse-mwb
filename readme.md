# parse-mwb

Parse file MySQL Workbench (Mwb to object) .


## Installation

```shell
yarn add gitlab:yt-lib/parse-mwb#semver:v2.x
```

## Importing

```js
import ParseMwb from 'parse-mwb' // ES6
const ParseMwb = require('parse-mwb') // ES5 with npm
```


## Usage

```ts
import * as path from 'path'
import ParseMwb from 'parse-mwb'

const parse = async () => {
  const tables = await ParseMwb.parseAsync(path.join(__dirname, 'documents/db.mwb'))
  console.log(tables[1].columns[0])
}
parse()
```
======= Happy done =======
